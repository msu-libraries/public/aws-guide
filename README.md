# AWS Guide

Documentation on getting started with and using AWS (Amazon Web Services)


## Table of Contents
* [Getting Started](#getting-started)
* [Service Overview](#service-overview)
   * [Elastic Beanstalk](#elastic-beanstalk)
   * [EC2](#ec2)
   * [Lightsail](#lightsail)
   * [Lambda](#lambda)
   * [Batch](#batch)
   * [S3](#s3)
   * [CodePipeline](#codepipeline)
   * [CodeCommit](#codecommit)
* [Practical Examples](#practical-examples)
   * [Setting up a deployment pipeline for a PHP Site](#setting-up-a-deployment-pipeline-for-a-php-site)
   * [Run code on Lambda](#run-code-on-lambda)
   * [Creating and using a database](#creating-and-using-a-database)
   * [Host a .NET Application](#host-a-net-application)


## Getting Started 
AWS comes in different "tiers" of service. Anyone can get the free tier account and most 
usage will not cost them anything. This changes if you start using it heavily, so make sure to 
monitor your usage through the [Console](https://console.aws.amazon.com). For more details 
on what is covered in the free tier, see the official [AWS Free Teir Details](https://aws.amazon.com/free/) 
page.

To get started, create a new free tier account on the [AWS Signup](https://portal.aws.amazon.com/billing/signup) page 
using your personal email address. It will prompt you for a credit card, but should not charge it 
unless you go over the limits specified for the free tier.

Once the account is created, you will be directed to the [Console](https://console.aws.amazon.com) 
which will show you all of the services now available to you. 

The next section will walk you through some of the services available (but not all of them). 

## Service Overview
This section only mentions a few of the services available through AWS. For a complete 
overview of all their services, see the [AWS Overview](https://d1.awsstatic.com/whitepapers/aws-overview.pdf) guide.

### Elastic Beanstalk
Used to run and manage web apps without having to worry about the underlying server configuration. 
It will increase/decrease it's configurations to meet the needs of your application. 

It suppoers services developed with Java, .NET, PHP, Node.js, Python, Ruby, Go,
and Docker hosted on servers using: Apache, Nginx, Passenger, and IIS. 

### EC2
Elastic Compute Cloud (EC2) provides compute capacity in the cloud. Allows for quick scalability as you 
can easily add and remove new server instances ( called EC2 instances) . 

### Lightsail
This is the lightweight way to launch and manage a virtual server. It includes a VM, SSD based storage, 
data transfer, DNS management, and a static IP address. But note that this is a paid service and not 
part of the free tier. 

### Lambda
Lambda lets you run code without even needing to setup or manage a server and you only pay for the 
compute time that you use. All you need to do is upload you code and it will take care of 
everything it needs to do to run it. You can even have your code automatically trigger these 
jobs from other services using the API. 

### S3
S3 is a storage service that offers high scalability, availability, security, and performance. 
It can be interacted with directly or via the API from other services. 

### CodePipeline
CodePipeline allows you to easily automate build, test, and deployment steps on your code. 
This allows for more rapid delivery of features. You can easily intergrate this with 
GitHub and other AWS services such as Elastic Beanstalk. See the 
[setting up a deployment pipeline](#setting-up-a-deployment-pipeline) example below for a step-
by-step guide on doing just that. 

### CodeCommit
This is a source control service that hosts git-based repositories. This eliminates the need 
to have to operate and maintain your own source control system. In most cases this is not 
relavent as a separate GitLab or GitHub server will be available. This guide will not go 
into more detail using CodeCommit for that reason.  

### RDS
This is a relational database service, which uses PostgreSQL. It basically allows you to have 
databases without having to operate and maintain a database server (and worrying about backups, 
failovers, etc.). 

## Practical Examples
Amazon provides many tutorials as well. A lot of what is in this guide is based off of 
using those tutorials. [AWS 10-Minute Tutorials](https://aws.amazon.com/getting-started/tutorials/).

### Setting up a deployment pipeline for a PHP Site
This is based off the guide provided by Amazon at: https://aws.amazon.com/getting-started/tutorials/continuous-deployment-pipeline/.
This section includes how to pull it all together since the interface has changed a bit since 
the guide was written. 

* Open the [AWS Elastic Beanstalk Console](https://us-east-1.console.aws.amazon.com/elasticbeanstalk)
* Click on `Create New Application` in the upper corner
* Provide a name, such as `testApp` and click `Create`
* Once in the application, click `Actions` -> `Create Environment`
* Select `Web server environment` for the first page
* You can leave the default values in the Environment information section
* For the Base configuration section, select `PHP` in the preconfigured platform field. The 
rest of the fields can be left to their defaults and you can click `Create environment`. 
This will take a few minutes to complete. But you can move on to the next step while that 
is working.
* Using GitHub you can either use one of your existing PHP applications, or fork the 
[example application](https://github.com/aws-samples/aws-codepipeline-s3-codedeploy-linux) used in the tutorial. If forking, on the GitHub page click `Fork` in the upper corner to copy it 
to your own GitHub account. 
* Now we need to set up the code pipeline that will automatically take that code and 
deploy it to the test environment we created above. Open the [AWS CodePipeline Console](http://console.aws.amazon.com/codepipeline) and click `Create pipelinIf forking, on the GitHub page click `Fork` in the upper corner to copy it 
to your own GitHub account. 
* Now we need to set up the code pipeline that will automatically take that code and 
deploy it to the test environment we created above. Open the [AWS CodePipeline Console](http://console.aws.amazon.com/codepipeline) and click `Create pipeline`.
* Privode a name for the pipeline, for example `DemoPipeline`. The default options for 
the rest of the fields on this page are fine. Click `Next` to continue. 
* For the source provider, click `GitHub`. It will prompt you to `Connet to GitHub` in the 
next field of the form.
* Next you will select the repository and branch that you want to use for this demo. 
* For the final option, leave it as GitHub webhooks since this will automatically trigger 
the pipeline when changes occur instead of just a periodic check. 
* For the Build provider page, skip over this and click `Skip build stage` since this 
demo application does not require any special build steps.
* For the Deploy stage, select `AWS Elastic Beanstalk` as the provider. Then select 
the application and environment we created earlier. 
* The first time you create a pipeline it will ask you to set up a service account 
that will have access to the relavent resources. For the `Service Role` field, click 
`Create role` and select `Allow` before continuing to the `Next step`.
* It will then prompt you to confirm your selections before clicking `Create pipeline`. 
This will take a minute and assumes your environment was created (which it should be 
by the time you've gotten to this step).
* It will deploy the initial set of code, and we can verify it using the URL at the top 
of the Elastic Beanstalk environment page.
* Now let's test committing a change to the code to make sure it automatically deploys. 
Using whatever method you feel most comfortable with, push a change to the GitHub 
repository. Either edditting it in the GitHub browser page or cloning it locally.
* The [AWS CodePipeline Console](http://console.aws.amazon.com/codepipeline) page will 
show you the status of the pipeline that should have automatically kicked off. Once it 
shows as complete, recheck the URL for your application to make sure the change has 
been successfully deployed.

#### Terminating the environment
As with all the examples, it is important to fully clean up your environment 
to avoid getting changed for services you are not actively using. 
* First delete the pipeline from the [AWS CodePipeline Console](http://console.aws.amazon.com/codepipeline). Nagivate to `Pipelines` then select the one you created and click `Delete pipeline`. It will ask you to confirm the deletion.
* Now delete the application from the [AWS Elastic Beanstalk Console](https://us-east-1.console.aws.amazon.com/elasticbeanstalk). Navigating to the `All Applications` page will have the 
option to select your application then go to `Actions` and `Delete application`. 
* You can optionally delete the code from your GitHub account as well by going to the 
`Settings` page in the repository and clicking `Delete this repository` near the bottom. 

### Run code on Lambda
This guide is based off of this tutorial provided by Amazon: https://aws.amazon.com/getting-started/tutorials/run-serverless-code/?trk=gs_card. 
* Open the [AWS Lamba Console](https://console.aws.amazon.com/lambda/). Click on `New Function`. 
* Select `Blueprints` and filter for `hello-world-python` and select the python3 example.
* On the configuration page, provide a name such as myTestFunction.
* For the Role field, select `Create a new role from one or more templates` and provide a name such as `lambda_basic`. Leave the Policy templates field blank.
* You can optionally make any change to the code you want and when you're ready, 
click on `Create function`. 
* Scroll down to the `Fucntion code` section and you can make sure the runtime is set to 
`Pythong 3.6`. The Handler field should be the function that you want execution to begin at. 
It should already be set to the correct function. 
* All the other default values on the page should be fine for the scope of this example, 
so you are now ready to click on `Test` to run the function to `Configure test event`. 
* For this example, choose `Hellow World` from the event templates and provide it a name such as `HelloWorldEvent`. and click `Create`.
* Now we can run the test by clicking `Test` with our event selected in the dropdown. 
Once it is complete you can click the `details` section of the execution results and it 
will tell you the results for each input as well as information about how long it 
took and how much resources it used. Once it has been run a few times, the `Monitoring` 
tab will show metrics over time. 
* The difference between the keys passed into the test function and the keys passed 
into the environment variables is that environment variables are typically used 
for runtime configurations such as database connection information. Where are test keys 
are used for sample input values. So let's change the code use use an environment 
variable as an example. 
* Create an environemnt varaiable called `test_var` with a value of `Hello World`. 
* In code the add the lines `import os` and `print("test_var = " + os.environ['test_var'])` 
* Save the changes and then click `Test` again to run it. You will see the output 
now also includes `Hello World` being printed. 

#### Running Code from the command line
Running code through the browser is a good starting point. But in most cases, you 
will have you code on a computer/server that you want to run without opening a 
browser window to do so. First of all, we need to set up the AWS CLI which 
can run on Linux, Windows, and macOS. 

* Install the CLI on Linux. (For all installation methods see: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
```
sudo apt install awscli
```

* Install the CLI on Windows via Cygwin
```
python3 -m ensurepip
python3 -m pip install awscli --upgrade --user
```

* From here on, the guide assumes you did the Linux installation method and prefaces 
all commands with `aws`, but if using the Windows method, make sure your commands 
begin with `python3 -m awscli` instead.

* Verify the install by making sure this returns a valid result
```
aws --version
```

* Now we need to create a new user that has the ability to run the function. From the 
[AWS IAM Console]() click `Add user`. Give it a name such as `execute_lambda` and 
select `Programmatic access` for the access type. 
* Create a new group called `execute_lambda` and filter the policies for `AWSLambdaBasicExecutionRole` and `AWSLambdaRole`. 
And continue to the next page.
* You can skip by the tags page since that will not be used in this example. And click `Create`* Once it is created it will 
show you and Access Key ID and Secret Access Key, which you will 
need for the next step. 
* Now we need to configure the CLI environment by running the `configure` command
```
aws configure
AWS Access Key ID [None]: [Paste key ID here]
AWS Secret Access Key [None]: [Paste key here]
Default region name [None]: [Region used in creating lambda function, see param in url]
Default output format [None]:
```
* Next we can try runing the function. Update the code in the function to match 
[the code included with this repository](https://gitlab.msu.edu/schanzme/aws-guide/blob/master/samples/python/lambda_function.py). 
This is because print values are not captured during command line runs and need to be a return value. 
```
aws lambda invoke --function-name myTestFunction \
--payload '{"key1":"value1", "key2":"value2", "key3":"value3"}' \
output.txt
```
* The `payload` field provides data to the `event` parameter of the function, 
similar to what the test you set up in the browser does. Not providing that data 
will result in an error.
* When the function runs, the return values will be placed in the specified output file 
and the print values are lost. What is printed to the screen is just the run status 
information such as success/failure and any error information not included in the 
output file. 
* If this returns an error about max retried exceded that probably means you are trying 
from a computer that is behind a firewall that is blocking the request. 
* Next lets try changing an environment variable.
```
aws lambda update-function-configuration --function-name myTestFunction --environment Variables={test_var=Hi}
```
* If you run it again now, it will use that new value. 
* As a final step, let's try changing the code locally and pushing the new code to Lambda 
without having to use the browser interface. Firstly, grab a copy of the [code from this 
repository](https://gitlab.msu.edu/schanzme/aws-guide/blob/master/samples/python/lambda_function.py), 
or copy-pasting from the Lambda console function code, and save it locally. 
* Make any change to the code, such as what the return statement string is
* Update the group in [IAM](https://console.aws.amazon.com/iam/home) to attach the 
policy `AWSLambdaFullAccess` which is required to push code changes. 
* Zip up the code and send it to Lambda
```
zip lambda_function.zip lambda_function.py
aws lambda update-function-code --function-name myTestFunction --zip-file fileb://function.zip
```
* Now run it again to make sure the changes were captured.


#### Terminating the environment
The only thing that needs to be removed once this example is complete is the function. 
* Open the [AWS Lamba Console](https://console.aws.amazon.com/lambda/) and go to 
the `Functions` tab on the left and select your function and go to `Actions` -> 
`Delete`.  
* Optionally, you can clear out the user and group you created for the CLI example, 
but it is not required. 

### Creating and using a database
This is based off the guide provided by Amazon at: https://aws.amazon.com/getting-started/tutorials/create-mysql-db/?trk=gs_card. 
This section includes how to connect to the database via your code and interact with it.

#### Setup and Execution
* Open the [Amazon AWS RDS Console](https://console.aws.amazon.com/rds/home)
* Click on the button to `Create a database`
* Select `MariaDB` (or `MySQL` if you prefer) and click `Next`
* The default options on the Instance specifications section are fine since you do want the latest version
* For the Settings section, provide a unique instance identifier (this is only referenced in the management console)
* Chose a mast username and password, make sure you remember these since you will need it when connecting to the database later
* For the Network & Security section, leaving the default options is fine.
* In Database options, provide a name for your database and remember it since you will need it when you connect to the database
* The only other setting to change on this page is `Deletion protection` which you will set to `off` since we will be removing this 
database right after we're done with this sample.
* Creating the database will take a few minutes. When it is done it will show you the information you need to connect to it. 
If the page does not automatically appear you can get to it anytime from the [Amazon AWS RDS Console](https://console.aws.amazon.com/rds/home) 
and then going to `Databases` and finding your instance name. 
* Using the [sample code](https://gitlab.msu.edu/schanzme/aws-guide/blob/master/samples/python/db_example.py), update the variables in
the first section with what you entered during the database creation and the endpoint (i.e. hostname) on the RDW Console page. 
* Run the code to make sure it worked.
```
$ python db_example.py
(1, 'record inserted.')
(u'John', u'Highway 21')
```

#### Terminating the Environment
To avoid charges to your account, you need to delete the database instance once you are done testing this sample.
* Navigate to the [Amazon AWS RDS Console](https://console.aws.amazon.com/rds/home) and then go to `Databases` and find 
your instance name. 
* Under `Actions` select `Delete`
* It will prompt you to confirm the deletion, make sure you check the box saying you don't need a final snapshot saved.
* The deletion will take a few minutes, but then it should disappear from the [Amazon AWS RDS Console](https://console.aws.amazon.com/rds/home) 
page. 

### Host a .NET Application
This is based off of the guide provided by Amazon at: https://aws.amazon.com/getting-started/projects/host-net-web-app/?trk=gs_card. 
These steps show how to put it all together for an easy to setup guide. 

* Install Visual Studio, available for free from the [Visual Studio Community](https://visualstudio.microsoft.com/vs/community/) 
page. 
* Next you will need to create a new AWS user that will be able to deploy the code. This is important 
since it is not recommended to use your primary root account for this. Navigate to the 
(Identity and Access Management Console)[https://console.aws.amazon.com/iam] and Click the `User` tab 
and then `Add User`. 
* You will want to give it a name meaningful to you, for example `[your username]_app` to indicate it is 
the application user. 
* Next select `Programmatic Access` and hit `Next`.
* Now you need to identify the permissions the user should have. For now, give it `AWSElasticBeanstalkFullAccess` 
access so it will have the ability to fully manage the Elastic Beanstalk environment you create. 
* The next page will prompt you to select or create a group for that user to reside within. You can call
it something like `app_users`. 
* Once the user is created it will show you the information you need to enter into Visual Studio to connect 
to AWS.
* So now we need to install the toolkit for Visual studio, which is available for download from: 
[VS toolkit for AWS](https://aws.amazon.com/visualstudio/). 
* Launch Visual Studio once the AWS Toolkit is installed and the user is created. It should prompt you 
to create a new profile, but if it doesn't it will appear on the AWS Explorer bar on the left by 
clicking `New Account Profile`. The profile is just how it identifies how it will connect to AWS, and in 
this case, we want to use the IAM user account we just created. Leaving it with the profile name `default` 
is fine. 
* It is important to note the region it is connecting to since the AWS URLs will differ by region. You 
can easily get tripped up thinking something does not exist when really it does in the other region. So 
if some instance or environment is missing from your AWS Explorer that you expected to appear, the first 
step is to check for the correct region (otherwise make sure the permissions of your profile are set correctly).
* Now that we have Visual Studio all set up to interact with AWS, lets setup a new project to deploy. The next 
section will walk through 2 examples, one that has just a basic .NET Core website and the other that 
actually interacts with AWS in the code (which requires a few extra steps to work). 

#### Basic .NET Core project
* Open Visual Studio and create a new project from a template, select `ASP.NET Core Web Application`.
* Run the code locally to make sure it executes without errors.
* In the Solution Explorer, right click on your 
project and click `Publish to AWS Elastic Beanstalk...`
* Select the correct account profile and region, they should be correct by default
* Leave `Create a new application environment` checked and click `Next`. You will 
use `Redeploy to an existing environment` whenever you have code changes you want 
to deploy.
* Choose a new for the application and environment, typically named after the 
project name in Visual Studio.
* If desired, change the autogenerated URL and `check availability...` to finalize 
the URL to use before clicking `Next`.
* The default EC2 Launch Configurations should be fine, Windows Server running IIS, 
with a micro instance type (these are small, cheaper to run environments).
* You will not need a Key pair for this example, but creating one now would allow you 
to directly SSH into the server created by this process.
* On the next page, the `Service Permissions` and `Deployed Application Permissions` 
should be fine with the default values since this application does not need to 
access any AWS services.
* The default Build and IIS deployment Settings should be fine since they are 
populated from your project's build settings.
* On the last page you can review your settings and click `Deploy`. This will take 
a few minutes to run (~10 minutes) since it has to create the whole enviornment. 
The environment status page that appears after starting the deploy will update with 
the status and provide you the URL to click on once it is complete to see your site. 
* Verify that the results on the page are the same as when you run it locally.
* Now anytime you make changes to the code you go through the same process, but can 
now just selct the `Redeploy to an existing environment` option. 
* The AWS Explorer bar on the left side will show you all of your Elastic Beanstalk 
applications as well as any other resources your profile has access to and allow for 
easy management without having to go to the [AWS Management Console](https://console.aws.amazon.com/console/). 

#### .NET Web Application that interacts with AWS
* Since this project will require access to interact with your AWS environment, we need to create a `Role` 
that can be used to deploy the code with which will have access to perform the actions used in the code. 
* Navigate to the 
(IAM Console)[https://console.aws.amazon.com/iam] and Click the `Roles` tab 
and then `Create Role`. 
* Select `AWS service` to allow it to be used by your AWS services and select `Elastic Beanstalk` for the
service that will use the role. 
* For this example we need to select `Elastic Beanstalk - Customizable` to give additional permissions. The 
default policies on the next page are fine as-is and you can click `Next`.
* There are no required tags at this time so you can continue to the next page.
* Provide a name for the role such as `beanstalk_app_role` and click `Create role` to complete.
* Back in Visual Studio, create a new project from an AWS Samples template. Select the `AWS Web Project`. 
Alternatively you can create an empty Web App project and add the code to that. 
* There are 3 files that you should review (or add the code to if you created a blank project):  
    * [Web.config](https://gitlab.msu.edu/schanzme/aws-guide/blob/master/samples/net/ec2_s3_connect/Web.config.example)
    * [Default.aspx](https://gitlab.msu.edu/schanzme/aws-guide/blob/master/samples/net/ec2_s3_connect/Default.aspx)
    * [Default.aspx.cs](https://gitlab.msu.edu/schanzme/aws-guide/blob/master/samples/net/ec2_s3_connect/Default.aspx.cs)
* Run the code locally to make sure it works as expected. 
* Now we're ready to deploy it to AWS! In the Solution Explorer, right click on your 
project and click `Publish to AWS Elastic Beanstalk...`
* Select the correct account profile and region, they should be correct by default
* Leave `Create a new application environment` checked and click `Next`. You will 
use `Redeploy to an existing environment` whenever you have code changes you want 
to deploy.
* Choose a new for the application and environment, typically named after the 
project name in Visual Studio.
* If desired, change the autogenerated URL and `check availability...` to finalize 
the URL to use before clicking `Next`.
* The default EC2 Launch Configurations should be fine, Windows Server running IIS, 
with a micro instance type (these are small, cheaper to run environments).
* You will not need a Key pair for this example, but creating one now would allow you 
to directly SSH into the server created by this process.
* On the next page, the `Service Permissions` section should be set the the role you 
had already created earlier. 
* For the `Deployed Application Permissions`, this is important since this is what 
sets the permissions when the application is running in the delopyed environment. 
For this example we are going to click on the dropdown and use the `Power User` 
template so that it will have access to retrieve the information needed in the `.cs` 
file.
* The default Build and IIS deployment Settings should be fine since they are 
populated from your project's build settings.
* On the last page you can review your settings and click `Deploy`. This will take 
a few minutes to run (~10 minutes) since it has to create the whole enviornment. 
The environment status page that appears after starting the deploy will update with 
the status and provide you the URL to click on once it is complete to see your site. 
* Verify that the results on the page are the same as when you run it locally, this 
means that the role permissions you set up are correct and are being used by the 
deployed application. 
* Now anytime you make changes to the code you go through the same process, but can 
now just selct the `Redeploy to an existing environment` option. 
* The AWS Explorer bar on the left side will show you all of your Elastic Beanstalk 
applications as well as any other resources your profile has access to and allow for 
easy management without having to go to the [AWS Management Console](https://console.aws.amazon.com/console/). 

### Terminating the Environment
The final step for both projects is to clean up your resources to avoid being charged for them.

You can do this through the AWS Explorer bar within Visual Studio, or you can do it directly 
in the [AWS Management Console](https://console.aws.amazon.com/console/).

Either right click on the AWS Elastic Beanstalk application you created within the 
AWS Explorer and click `Delete` or go to the [Console](https://console.aws.amazon.com/console/)] 
and open up AWS Elastic Beanstalk then under `Actions` select `Terminate Application`. 

Again here it is important to be aware of the region since applications, environments, and 
instances only show up for the region they were created in. In the AWS Explorer you can specify 
this by using the `Region` dropdown. In the [AWS Management Console](https://console.aws.amazon.com/console/)
this is at the end of the url, for example: https://console.aws.amazon.com/elasticbeanstalk/home?region=us-east-1. 
