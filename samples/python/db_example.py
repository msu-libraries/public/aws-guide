#!/usr/bin/env python3

# Setup Steps:
# pip install mysql-connector

import mysql.connector

mydb = mysql.connector.connect(
  host="[DB Host]",
  user="[DB User]",
  passwd="[DB Password]",
  database='[DB Name]'
)


mycursor = mydb.cursor()

### Create a table
mycursor.execute("CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))")


### Add data to the table 
sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = ("John", "Highway 21")
mycursor.execute(sql, val)

mydb.commit()

print(mycursor.rowcount, "record inserted.")

### Get data from the table
mycursor.execute("SELECT * FROM customers")

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
