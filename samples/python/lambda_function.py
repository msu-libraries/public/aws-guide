import json
import os 

def lambda_handler(event, context):
    print("value1 = " + event['key1'])
    print("value2 = " + event['key2'])
    print("value3 = " + event['key3'])
    print("test_var = " + os.environ['test_var'])
    return os.environ['test_var'] + ". value1 = " + event['key1']
